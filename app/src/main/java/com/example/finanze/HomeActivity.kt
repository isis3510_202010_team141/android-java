package com.example.finanze

import android.Manifest.*
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.BatteryManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_auth.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.net.InetAddress
import java.text.SimpleDateFormat
import java.util.*
import java.util.jar.Manifest



enum class ProviderType(){
    BASIC
}
class HomeActivity : AppCompatActivity() {
    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null
    private val INTERVAL: Long = 2000
    private val FASTEST_INTERVAL: Long = 1000
    lateinit var mLastLocation: Location
    internal lateinit var mLocationRequest: LocationRequest
    private val REQUEST_PERMISSION_LOCATION = 10

    lateinit var sharedPreferences : SharedPreferences
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    var updateOn = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_auth)
        //analytics
        firebaseAnalytics = Firebase.analytics
        // setupLocation
        sharedPreferences = getSharedPreferences("gps", MODE_PRIVATE)
        mLocationRequest = LocationRequest()
        setupLocation()

        // setup
        setup()
    }

    private fun setup(){

        title = "Autentication"



        signUpButton.setOnClickListener {
            if (emailEdit.text.isNotEmpty() && passwordEdit.text.isNotEmpty()){
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                    emailEdit.text.toString(),
                    passwordEdit.text.toString()
                ).addOnCompleteListener {
                    if (it.isSuccessful){
                        GlobalScope.launch(Dispatchers.IO) {
                            var id: Int = 0
                            val email = it.result?.user?.email
                            val url = "django-middle-finance.herokuapp.com/users/"
                            val cache  = DiskBasedCache(cacheDir, 512 * 512)
                            val network = BasicNetwork(HurlStack())
                            val queue = RequestQueue(cache, network)
                            val jsonBody = JSONObject("{ \"email\":\"$email\"}")
                            val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, url, jsonBody,
                                {response ->
                                    val edit = sharedPreferences.edit()
                                    id = response.getInt("id")
                                    edit.putString("id", id.toString())
                                },
                                {err ->
                                    err.printStackTrace()
                                })
                            val jsonBodyOS = JSONObject("{\"user_id\": $id, \"os_version\":${Build.VERSION.SDK_INT}, \"isAndroid\": true}")
                            val urlOS = ""
                            val jsonOSRequest = JsonObjectRequest(Request.Method.POST, urlOS, jsonBodyOS,
                                {

                                },
                                {

                                }
                            )
                            queue.add(jsonObjectRequest)
                            queue.add(jsonOSRequest)
                        }
                        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP) {
                            param(FirebaseAnalytics.Param.ITEM_NAME, "Sign In")
                            param(FirebaseAnalytics.Param.CONTENT_TYPE, "User")
                        }

                        showHome(it.result?.user?.email ?:"", ProviderType.BASIC)
                    }else{
                        showAlert()
                    }
                }
            }
        }

        logInButton.setOnClickListener {
            if (emailEdit.text.isNotEmpty() && passwordEdit.text.isNotEmpty()){
                FirebaseAuth.getInstance()
                    .signInWithEmailAndPassword(
                        emailEdit.text.toString(),
                        passwordEdit.text.toString()).addOnCompleteListener {
                        if (it.isSuccessful){
                            GlobalScope.launch(Dispatchers.IO) {
                                val email = it.result?.user?.email
                                val url = "django-middle-finance.herokuapp.com/client/$email/"
                                val cache  = DiskBasedCache(cacheDir, 512 * 512)
                                val network = BasicNetwork(HurlStack())
                                val queue = RequestQueue(cache, network)
                                val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null,
                                    {response ->
                                        val edit = sharedPreferences.edit()
                                        edit.putString("id", response.getInt("id").toString())
                                    },
                                    {err ->
                                        err.printStackTrace()
                                    })
                                queue.add(jsonObjectRequest)
                            }
                            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN) {
                                param(FirebaseAnalytics.Param.ITEM_NAME, "Log In")
                                param(FirebaseAnalytics.Param.CONTENT_TYPE, "User")
                            }
                            showHome(it.result?.user?.email ?:"", ProviderType.BASIC)
                        }else{
                            showAlert()
                        }
                    }
            }
        }
    }

    private fun setupLocation(){
        val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            buildAlertMessageNoGps()
        }

        if (checkPermissionForLocation(this)){
            startLocationUpdates()
            val edit = sharedPreferences.edit()
            edit.putString("gpsPermission", "true")
            edit.apply()
        }

    }

    private fun getBatteryLevel(context: Context, intent: Intent?): Float {
        val batteryStatus: Intent? = context.registerReceiver(
            null,
            IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        )
        var batteryLevel = -1
        var batteryScale = 1
        if (batteryStatus != null) {
            batteryLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, batteryLevel)
            batteryScale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, batteryScale)
        }
        return batteryLevel / batteryScale.toFloat() * 100
    }

    private fun showAlert() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        if(isInternetAvailable()){
            builder.setMessage("Authentication Failed")
        }
        else{
            builder.setMessage("You need to be connected to a network to enter.")
        }
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    fun isInternetAvailable(): Boolean {
        return try {
            val ipAddr: InetAddress = InetAddress.getByName("google.com")
            //You can replace it with your name
            !ipAddr.equals("")
        } catch (e: Exception) {
            false
        }
    }

    private fun showHome(email:String, provider: ProviderType){
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_transactions, R.id.navigation_notifications
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

    }

    private fun buildAlertMessageNoGps() {

        val builder = AlertDialog.Builder(this)
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id ->
                startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    , 11)
            }
            .setNegativeButton("No") { dialog, id ->
                dialog.cancel()
                finish()
            }
        val alert: AlertDialog = builder.create()
        alert.show()
    }

    protected fun startLocationUpdates() {

        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest!!.setInterval(INTERVAL)
        mLocationRequest!!.setFastestInterval(FASTEST_INTERVAL)

        // Create LocationSettingsRequest object using location request
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest!!)
        val locationSettingsRequest = builder.build()

        val settingsClient = LocationServices.getSettingsClient(this)
        settingsClient.checkLocationSettings(locationSettingsRequest)

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        mFusedLocationProviderClient!!.requestLocationUpdates(mLocationRequest, mLocationCallback,
            Looper.myLooper())
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            // do work here
            locationResult.lastLocation
            onLocationChanged(locationResult.lastLocation)
        }
    }

    fun onLocationChanged(location: Location) {
        // New location has now been determined

        mLastLocation = location
        val edit = sharedPreferences.edit()
        edit.putString("lat", mLastLocation.latitude.toString())
        edit.putString("lng", mLastLocation.longitude.toString())
        edit.apply()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSION_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationUpdates()
            } else {
                Toast.makeText(this@HomeActivity, "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun checkPermissionForLocation(context: Context): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (context.checkSelfPermission(permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
                true
            } else {
                // Show the permission request
                ActivityCompat.requestPermissions(this, arrayOf(permission.ACCESS_FINE_LOCATION),
                    REQUEST_PERMISSION_LOCATION)
                false
            }
        } else {
            true
        }
    }


}
