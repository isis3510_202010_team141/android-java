package com.example.finanze

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.finanze.ui.transactions.TransactionViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_new_transaction.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val IN = "Income"
private const val OUT = "Outcome"


class NewTransactionFragment : Fragment() {

    private lateinit var tranViewModel: TransactionViewModel
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var sharedPreferences : SharedPreferences? = null
    var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
        sharedPreferences = this.activity?.getSharedPreferences("gps", Context.MODE_PRIVATE)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        tranViewModel = ViewModelProvider(this).get(TransactionViewModel::class.java)

        cancel_button.setOnClickListener {
            requireActivity().onBackPressed()
        }

        add_button.setOnClickListener {
            if(nameTransactEdit.text.isNotEmpty() && valueTransacEdit.text.isNotEmpty() && type_group.checkedRadioButtonId!= -1){
                val name = nameTransactEdit.text.toString()
                val typeText = view.findViewById<RadioButton>(type_group.checkedRadioButtonId).text
                val amount = valueTransacEdit.text.toString().toDouble()
                var type = false
                if(typeText == OUT){
                    type = true
                }
                val id = sharedPreferences?.getString("id", "1")?.toInt()
                if (id != null) {

                    tranViewModel.newTransaction(name, type, amount, id)
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                        param(FirebaseAnalytics.Param.ITEM_NAME, name)
                        param(FirebaseAnalytics.Param.CONTENT_TYPE, "Transaction")
                    }

                }
                else{
                    tranViewModel.newTransaction(name, type, amount, 0)
                }
                nameTransactEdit.setText("selescted: " + type)
                requireActivity().onBackPressed()
            }
        }
    }
    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            NewTransactionFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}