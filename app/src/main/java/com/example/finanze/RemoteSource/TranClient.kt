package com.example.finanze.RemoteSource

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class TranClient {

    private val tService: TransactionService
    private val retrofit: Retrofit

    companion object {
        var instance: TranClient? = null
            get() {
                if(field == null){
                    instance = TranClient()
                }
                return field
            }
    }

    init {

        retrofit = Retrofit.Builder()
            .baseUrl("https://django-middle-finance.herokuapp.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        tService = retrofit.create(TransactionService::class.java)

    }

    fun getTService() = this.tService
}