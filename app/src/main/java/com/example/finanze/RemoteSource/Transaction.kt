package com.example.finanze.RemoteSource

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "transaction_table")
data class Transaction(

    @PrimaryKey(autoGenerate = true) var id: Int,

    var client: Int,
    var date: String,
    var name: String,
    var typeOf: Boolean,
    var value: String
) {


}