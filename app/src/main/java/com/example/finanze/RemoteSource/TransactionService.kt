package com.example.finanze.RemoteSource

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface TransactionService {

    @GET("tr/{userId}/{page}")
    fun getTransactions(@Path("userId") userId: Int, @Path("page") page: Int): Call<TransactionResponse>
}