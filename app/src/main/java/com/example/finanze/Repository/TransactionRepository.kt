package com.example.finanze.Repository

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkInfo
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.finanze.RemoteSource.TranClient
import com.example.finanze.RemoteSource.Transaction
import com.example.finanze.RemoteSource.TransactionResponse
import com.example.finanze.RemoteSource.TransactionService
import com.example.finanze.RoomDB.DAO
import com.example.finanze.common.MyApp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TransactionRepository(private val dao: DAO) {




    // RemoteSource References
    var remoteTService: TransactionService? = null
    var remoteTClient: TranClient? = null

    // Transactions:
    var allTransactions: LiveData<List<Transaction>> = dao.getTransactions()
    var allRemoteTransactions: MutableLiveData<List<Transaction>>? = null

    var pag = 1
    var userID = 2


    init {
        remoteTClient = TranClient.instance
        remoteTService = remoteTClient?.getTService()
        allTransactions = allTr()


        //allRemoteTransactions = getRemoteTransactions()

    }
    private fun allTr(): LiveData<List<Transaction>> {
        if(allTransactions == null){
            allTransactions = dao.getTransactions()
        }


        return allTransactions
    }
    suspend fun insert(transaction: Transaction){

        dao.insert(transaction)
    }


    suspend fun getRemoteTransactions() {
        if (isOnline()){
            withContext(Dispatchers.IO) {
                val data = MutableLiveData<List<Transaction>>()

                val call: Call<TransactionResponse>? = remoteTService?.getTransactions(1, 1)
                call?.enqueue(object : Callback<TransactionResponse> {
                    override fun onResponse(
                        call: Call<TransactionResponse>,
                        response: Response<TransactionResponse>
                    ) {
                        if (response.isSuccessful) {
                            //var tr= toTEntity(response.body())
                            data.value = response.body()

                            // Sincronizar transacciones

    //                        for (t in data.value!!) {
    //                            dao.insert(
    //                                TransactionEntity(
    //                                    t.id,
    //                                    t.name,
    //                                    t.getDate(),
    //                                    t.value.toDouble()
    //                                )
    //                            )
    //                        }

                        }
                    }

                    override fun onFailure(call: Call<TransactionResponse>, t: Throwable) {
                        Toast.makeText(MyApp.instance, "Server error", Toast.LENGTH_LONG).show()
                    }

                })

                allRemoteTransactions = data
            }
        }
        else{
            Toast.makeText(MyApp.instance, "Check your connectivity and try again.", Toast.LENGTH_LONG).show()
        }

    }

    fun isOnline(): Boolean {
        val cm = MyApp.instance.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        return isConnected
    }

//    private suspend fun cacheTransactions()  {
//        var trs = allRemoteTransactions.value
//
//            if(allRemoteTransactions!=null) {
//                for (t in allRemoteTransactions?.value!!) {
//                    if(t!=null) {
//                        dao.insert(TransactionEntity(t.id, t.name, t.getDate(), t.value.toDouble()))
//                    }
//                    else{
//                        print("paila mano")
//                    }
//                }
//            }
//
//    }


}