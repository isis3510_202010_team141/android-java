package com.example.finanze.RoomDB

import java.io.Serializable
import java.util.*
data class Photo(var height: Int, var html_attributions: Array<String>, var photo_reference: String, var width: Int)
data class OpeningHours(var open_now: Boolean)
data class ATMEntity(var name: String, var opening_hours: Boolean?=false, var address: String) : Serializable
