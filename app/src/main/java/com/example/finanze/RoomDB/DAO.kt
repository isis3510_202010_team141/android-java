package com.example.finanze.RoomDB

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.finanze.RemoteSource.Transaction

@Dao
interface DAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(transaction: Transaction)

    @Query("SELECT * from transaction_table ORDER BY date")
    fun getTransactions(): LiveData<List<Transaction>>

    @Update
    fun updateTransaction(transaction: Transaction)

    @Delete
    fun deleteTransaction(transaction: Transaction)

    @Query("DELETE FROM transaction_table")
    fun deleteAll()

}