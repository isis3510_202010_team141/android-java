package com.example.finanze.RoomDB

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.finanze.RemoteSource.Transaction
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.security.AccessControlContext
import java.util.*

@Database(entities = [Transaction::class], version = 3)
//@TypeConverters(FinanceDatabase.Converters::class)
abstract class FinanceDatabase : RoomDatabase(){

    abstract fun dao(): DAO

    companion object {
        // Singleton
        @Volatile
        private var INSTANCE: FinanceDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): FinanceDatabase {
            return INSTANCE ?: synchronized(this){
                val tempInstance = Room.databaseBuilder(
                    context.applicationContext,
                    FinanceDatabase::class.java,
                    "finance_database"
                ).fallbackToDestructiveMigration().addCallback(TDatabaseCallback(scope)).build()
                INSTANCE = tempInstance
                tempInstance

            }

        }

        private class TDatabaseCallback(
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback(){

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.dao())
                    }
                }
            }

        }

        fun populateDatabase(tDao: DAO) {
            tDao.deleteAll()
//
//            for (i in 1 .. 20) {
//                var tran = TransactionEntity(0, "Comida", "$i-oct", 35600.0+i)
//                tDao.insert(tran)
//            }
        }
    }


}