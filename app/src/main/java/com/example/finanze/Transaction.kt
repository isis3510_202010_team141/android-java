package com.example.finanze

import java.util.*

data class Transaction(var title: String, var amount: Double, var date: GregorianCalendar)

object Supplier{

    val transactions = listOf<Transaction>(
        Transaction("Lunch", 10000.0, GregorianCalendar(2010, 12, 22, 10, 3)),
        Transaction("Break", 10000.0, GregorianCalendar(2010, 12, 22, 11, 4)),
        Transaction("Dinner", 10000.0, GregorianCalendar(2010, 12, 22, 12, 55)),
        Transaction("Samosa", 10000.0, GregorianCalendar(2010, 12, 22, 13, 12)),
        Transaction("Merengye", 10000.0, GregorianCalendar(2010, 12, 22, 15, 39))



    )
}