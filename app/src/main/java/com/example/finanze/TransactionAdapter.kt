package com.example.finanze

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_items.view.*

class TransactionAdapter(val context: Context, val transactions: List<Transaction>) : RecyclerView.Adapter<TransactionAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_items, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return transactions.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val transaction = transactions[position]
        holder.setData(transaction, position)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun setData(transaction: Transaction, position: Int){
            itemView.txvTitle.text = transaction.title
        }
    }
}