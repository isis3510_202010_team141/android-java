package com.example.finanze.ui.nearby_atm

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.finanze.R
import com.example.finanze.RoomDB.ATMEntity
import kotlinx.android.synthetic.main.fragment_atm_item.view.*
import java.io.File
import java.io.FileOutputStream

class AtmViewAdapter(private var atms: ArrayList<ATMEntity>) : RecyclerView.Adapter<AtmViewAdapter.ViewHolder>() {


    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ATMEntity
        }
    }
    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        val nameView: TextView = view.atm_name
        val addressView: TextView = view.atm_address
        val isOpenView: TextView = view.atm_isOpen
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AtmViewAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_atm_item, parent, false);
        return ViewHolder(view);

    }

    override fun getItemCount(): Int = atms.size

    internal fun setData(values: ArrayList<ATMEntity>) {
        this.atms = values
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: AtmViewAdapter.ViewHolder, position: Int) {
        val atm = atms[position]
        holder.nameView.text = atm.name
        holder.addressView.text = atm.address
        holder.isOpenView.text = if(atm.opening_hours!!) "OPEN" else "CLOSED"

        with(holder.itemView){
            tag = atm
            setOnClickListener(mOnClickListener)
        }

    }


}