package com.example.finanze.ui.nearby_atm

import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.example.finanze.R
import com.example.finanze.RoomDB.ATMEntity
import com.example.finanze.RoomDB.CustomJsonArrayRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.io.FileNotFoundException
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.net.InetAddress

class AtmsFragment : Fragment() {

    private lateinit var atmViewModel: AtmsViewModel
    private lateinit var atmViewAdapter: AtmViewAdapter
    private var sharedPreferences: SharedPreferences? = null
    private var atms: ArrayList<ATMEntity> = ArrayList()
    private var columnCount = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = this.activity?.getSharedPreferences("gps", Context.MODE_PRIVATE)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Reference to the List list view:
        val rootView = inflater.inflate(R.layout.fragment_atm_list, container, false)
        // Reference to the recyclable list:
        val recyclerListView = rootView.findViewById(R.id.atm_list) as RecyclerView

        // Obtain and initialize the The ViewModel and the Adapter.
        atmViewModel = ViewModelProvider(this).get(AtmsViewModel::class.java)
        atmViewAdapter = AtmViewAdapter(atms)

        // Set the adapter
        if (recyclerListView is RecyclerView) {
            with(recyclerListView) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(this.context)
                    else -> GridLayoutManager(this.context, columnCount)
                }
                adapter = atmViewAdapter
            }
        }

        val permission = sharedPreferences?.getString("gpsPermission", "false")
        if(!isInternetAvailable()) {
            try{
                val fis = context?.openFileInput("atms")
                val ois = ObjectInputStream(fis)
                atms = ois.readObject() as ArrayList<ATMEntity>
                atmViewAdapter.setData(atms)
                ois.close()
            }catch (e: FileNotFoundException){
                val dialogBuilder = AlertDialog.Builder(requireActivity())
                dialogBuilder.setMessage("You need to connect to  the internet to see the atms near you.")
                    // if the dialog is cancelable
                    .setCancelable(false)
                    .setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, id ->
                        dialog.dismiss()

                    })

                val alert = dialogBuilder.create()
                alert.setTitle("No internet connection")
                alert.show()
            }
        }
        if (permission.equals("true")) {
            Log.d("hello", "itsPermited")
            atmViewModel.viewModelScope.launch(Dispatchers.IO) {
                requestAtms()
            }
        }


        return rootView
    }


    protected fun requestAtms(){
        val cache  = DiskBasedCache(context?.cacheDir, 1024 * 1024)
        val network = BasicNetwork(HurlStack())
        val url = "https://express-finance.herokuapp.com/app/locations"
        val queue = RequestQueue(cache, network)

        val lat = sharedPreferences?.getString("lat", "4.601862")?.toDouble()
        val lng = sharedPreferences?.getString("lng", "-74.065044")?.toDouble()
        val id = sharedPreferences?.getString("id", "0")?.toInt()
        val jsonBody = JSONObject("{ \"coordinates\": { \"lat\": $lat, \"lng\":$lng }}")
        Log.d("gps", lat.toString())
        Log.d("gps", lng.toString())
        val jsonArrayRequest = CustomJsonArrayRequest(
            Request.Method.POST, url, jsonBody,
            Response.Listener {
                var length = it.length() - 1
                for (i in 0..length) {
                    var restaurant = it.getJSONObject(i)
                    //var openingHours = restaurant.getJSONObject("opening_hours")
                    atms.add(
                        ATMEntity(
                            name = (restaurant.get("name") as String) ?: "Test",
                            opening_hours = true,
                            address = restaurant.get(
                                "address"
                            ) as String
                        )
                    )
                }
                atmViewAdapter.setData(atms)
                try {
                    val fos = context?.openFileOutput("atms", Context.MODE_PRIVATE)
                    val oos = ObjectOutputStream(fos)
                    oos.writeObject(atms)
                    oos.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            },
            Response.ErrorListener {
                it.printStackTrace()

            }

        )
        val urlLoc = "https://backend-analytics-flask.herokuapp.com/usrloc"
        val jsonBodyLocation = JSONObject("{ \"LastLocation\": { \"lat\": $lat, \"lng\":$lng, \"user_id\":$id }}")
        val jsonLocationRequest = JsonObjectRequest(
            Request.Method.POST, urlLoc, jsonBodyLocation,
            {

            },
            {
                it.printStackTrace()
            }

        )
        queue.start()
        queue.add(jsonArrayRequest)
        queue.add(jsonLocationRequest)
    }

    fun isInternetAvailable(): Boolean {
        return try {
            val ipAddr: InetAddress = InetAddress.getByName("google.com")
            //You can replace it with your name
            !ipAddr.equals("")
        } catch (e: Exception) {
            false
        }
    }
}