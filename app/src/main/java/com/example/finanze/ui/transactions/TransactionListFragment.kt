package com.example.finanze.ui.transactions

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.finanze.R
import com.example.finanze.RemoteSource.Transaction
import com.google.android.material.floatingactionbutton.FloatingActionButton

class TransactionListFragment : Fragment(), View.OnClickListener {

    private lateinit var tranViewModel: TransactionViewModel
    private lateinit var tranAdapter: TransactionRecyclerViewAdapter
    private var transactions: List<Transaction> = ArrayList()

    private var columnCount = 1

    var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Reference to the List list view:
        val view = inflater.inflate(R.layout.fragment_transaction_list_list, container, false)
        // Reference to the recyclable list:
        val recyclerListView = view.findViewById<RecyclerView>(R.id.list)

        // Obtain and initialize the The ViewModel and the Adapter.
        tranViewModel = ViewModelProvider(this).get(TransactionViewModel::class.java)
        tranAdapter = TransactionRecyclerViewAdapter()

        // Set the adapter
        if (recyclerListView is RecyclerView) {
            with(recyclerListView) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = tranAdapter
            }
        }

        //
        tranViewModel.allTransactions.observe(viewLifecycleOwner, Observer {
            transactions = it
            tranAdapter.setData(transactions)
        })
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        view.findViewById<FloatingActionButton>(R.id.fab).setOnClickListener (this)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
                TransactionListFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                    }
                }
    }

    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.fab -> navController!!.navigate(R.id.action_navigation_transactions_to_newTransactionFragment2)
        }
    }
}