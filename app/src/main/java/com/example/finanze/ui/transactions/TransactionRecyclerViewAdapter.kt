package com.example.finanze.ui.transactions

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.finanze.R
import com.example.finanze.RemoteSource.Transaction
import kotlinx.android.synthetic.main.fragment_transaction_list.view.*
import java.text.DateFormat
import java.util.*
import kotlin.collections.ArrayList

class TransactionRecyclerViewAdapter ()
    : RecyclerView.Adapter<TransactionRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener
    private var transactions: List<Transaction> = ArrayList()

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Transaction
        }
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val nameView: TextView = view.item_name
        val dateView: TextView = view.item_date
        val valueView: TextView = view.item_value
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_transaction_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = transactions[position]
        holder.nameView.text = item.name
        holder.dateView.text = item.date
        holder.valueView.text = "$ " + item.value

        with(holder.itemView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    internal fun setTransactions(transactions: List<Transaction>){
        this.transactions = transactions
        notifyDataSetChanged()
    }

    internal fun setData(values: List<Transaction>?) {
        this.transactions = values!!
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = transactions.size


}