package com.example.finanze.ui.transactions

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.finanze.RemoteSource.Transaction
import androidx.preference.PreferenceManager
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import com.example.finanze.Repository.TransactionRepository
import com.example.finanze.RoomDB.FinanceDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList

class TransactionViewModel(application: Application) : AndroidViewModel(application) {
    private val repo: TransactionRepository

    val allTransactions: LiveData<List<Transaction>>

    init {
        val dao = FinanceDatabase.getDatabase(application, viewModelScope).dao()
        repo = TransactionRepository(dao)
        allTransactions = repo.allTransactions
        viewModelScope.launch {
            repo.getRemoteTransactions()
        }
    }

    fun insert(transaction: Transaction, id: Int) = viewModelScope.launch(Dispatchers.IO) {
        val name = transaction.name
        val value = transaction.value
        var typeOf = transaction.typeOf
        val date = transaction.date
        val url = "django-middle-finance.herokuapp.com/transactions/"
        val queue = Volley.newRequestQueue(getApplication())
        val jsonBody = JSONObject("{ \"client\": $id, \"name\":\"$name\", \"typeOf\": $typeOf, \"date\": \"$date\", \"value\":$value}")
        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST,url,jsonBody,
            {response ->

            },
            {
                it.printStackTrace()
            }
            )
        queue.start()
        queue.add(jsonObjectRequest)
        repo.insert(transaction)
    }

    fun getTransactions(): LiveData<List<Transaction>> {
        return allTransactions
    }

    fun getTransactionsByID(id: Int): MutableList<Transaction>{
        var transactionList: MutableList<Transaction> = ArrayList<Transaction>()
        val url = "django-middle-finance.herokuapp.com/trans/$id/"
        val queue = Volley.newRequestQueue(getApplication())
        val JsonArrayRequest = JsonArrayRequest(Request.Method.GET, url, null,
            {response ->
                val length = response.length() - 1
                for (i in 0..length){
                    var trans = response.getJSONObject(i)
                    var typeOf = trans.getBoolean("typeOf")
                    val add = transactionList.add(
                        Transaction(0, name = trans.getString("name"), date = trans.getString("date"), value = trans.getString("value"), typeOf = typeOf, client = id)
                    )
                }
            },
            {

            }
            )
        return transactionList
    }

    fun newTransaction(name: String, type: Boolean, value: Double, id: Int){
        val now = Date()
        val newT = Transaction(id = 0, name = name, value = value.toString(), date = now.toString(), client = repo.userID, typeOf = type)
        insert(newT, id)
    }

}